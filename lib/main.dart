import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:test_app/app/data/storage/hive.dart';

import 'app/app.dart';

Future<void> _initAppInitials() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Boxes.initBoxes();

  await ScreenUtil.ensureScreenSize();
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await _initAppInitials();

  runApp(const AppRoot());
}

class AppRoot extends StatelessWidget {
  const AppRoot({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: TestApp(),
    );
  }
}

class TestApp extends StatelessWidget {
  const TestApp({super.key});

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return ScreenUtilInit(
          designSize: Size(constraints.maxWidth, constraints.maxHeight),
          builder: (_, child) => GetMaterialApp(
            initialRoute: AppRoutes.loginScreen,
            getPages: AppPages.pageList,
            debugShowCheckedModeBanner: false,
            themeMode: ThemeMode.light,
          ),
        );
      },
    );
  }
}



// Hive model generation
// flutter packages pub run build_runner build --delete-conflicting-outputs
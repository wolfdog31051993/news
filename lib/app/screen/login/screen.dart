import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:test_app/app/app.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
          body: Center(child: Image.asset(MyAssetsImages.logoIc)),
        ),
        Positioned(
          bottom: MediaQuery.of(context).viewInsets.bottom + 50,
          left: 24,
          right: 24,
          child: Column(
            children: [
              PrimaryButton(onPressed: () {}, text: 'Войти через ЕЛК'),
              SizedBox(height: 16.h),
              PrimaryButton(
                onPressed: () => Get.toNamed(AppRoutes.onBoardingScreen),
                text: 'Войти без авторизации',
                color: ThemeColors.secondaryColor,
                textColor: ThemeColors.primaryColor,
              )
            ],
          ),
        )
      ],
    );
  }
}

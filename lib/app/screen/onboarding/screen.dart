import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_app/app/screen/screen.dart';

class OnBoardingScreen extends StatelessWidget {
  const OnBoardingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return GetX<OnBoardingController>(
      init: OnBoardingController(),
      builder: (oc) {
        List<Widget> pages = [
          FirstPage(oc: oc),
          SecondPage(oc: oc),
          const ThirdPage(),
        ];
        return Scaffold(
          body: Stack(
            children: [
              PageView(
                controller: oc.state.pageController,
                onPageChanged: (page) => oc.nextPage(page),
                physics: const ClampingScrollPhysics(),
                children: pages,
              ),
              Positioned(
                bottom: MediaQuery.of(context).viewInsets.bottom + 30,
                left: 24,
                right: 24,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: oc.state.buildIndicators(),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}

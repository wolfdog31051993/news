import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_app/app/core/core.dart';

class OnBoardingIndicator {
  static Widget indicator(bool isActive) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 150),
      height: 8.0,
      width: isActive ? 20.0.w : 8.0,
      margin: const EdgeInsets.only(right: 8, top: 10, bottom: 10),
      decoration: BoxDecoration(
          color: isActive ? ThemeColors.primaryColor : ThemeColors.uiWhiteColor,
          borderRadius: const BorderRadius.all(Radius.circular(12.0))),
    );
  }
}

import 'package:get/get.dart';
import 'package:test_app/app/screen/screen.dart';

class OnBoardingController extends GetxController {
  final state = OnBoardingState();

  void nextPage(int page) {
    state.currentPage.value = page;
  }

  void moveToNextPage() {
    state.pageController.jumpToPage(state.currentPage.value + 1);
  }
}

import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:test_app/app/screen/screen.dart';

class OnBoardingState {
  RxInt pageNumbers = 3.obs;
  RxInt currentPage = 0.obs;
  PageController pageController = PageController(initialPage: 0);

  List<Widget> buildIndicators() {
    List<Widget> pageIndicatorList = [];
    for (int i = 0; i < pageNumbers.value; i++) {
      pageIndicatorList
          .add((i == currentPage.value) ? OnBoardingIndicator.indicator(true) : OnBoardingIndicator.indicator(false));
    }
    return pageIndicatorList;
  }
}

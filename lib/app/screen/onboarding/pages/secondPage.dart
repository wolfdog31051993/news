import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../app.dart';

class SecondPage extends StatelessWidget {
  final OnBoardingController oc;
  const SecondPage({super.key, required this.oc});

  @override
  Widget build(BuildContext context) {
    var heightBothWidgets = SizedBox(height: 16.h);
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(image: AssetImage(MyAssetsImages.secondOnBoardingImg), fit: BoxFit.fill),
      ),
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topRight,
                child: GestureDetector(
                  onTap: () => Get.to(
                    () => const DashboardScreen(),
                    transition: Transition.upToDown,
                    duration: const Duration(milliseconds: 1000),
                  ),
                  child: Container(
                    height: 40.h,
                    width: 88.w,
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(right: 24.w),
                    decoration: BoxDecoration(
                      color: const Color(0xFFF2F3F5).withOpacity(0.6),
                      borderRadius: BorderRadius.all(
                        Radius.circular(12.r),
                      ),
                    ),
                    child: Text(
                      'Пропустить',
                      style: TextStyle(
                        fontSize: 12.sp,
                        fontWeight: FontWeight.w700,
                        fontFamily: Constants.fontName,
                        color: ThemeColors.uiWhiteColor,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
              Positioned(
                bottom: MediaQuery.of(context).viewInsets.bottom + 30,
                left: 24,
                right: 24,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Центр уведомлений',
                      style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontFamily: Constants.fontName,
                          fontSize: 24.sp,
                          color: ThemeColors.uiWhiteColor),
                      textAlign: TextAlign.center,
                    ),
                    heightBothWidgets,
                    Text(
                      'Сразу после наступления события, мы уведомим вас сообщением на мобильном устройстве ',
                      style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontFamily: Constants.fontName,
                          fontSize: 16.sp,
                          color: ThemeColors.uiWhiteColor),
                      textAlign: TextAlign.center,
                    ),
                    heightBothWidgets,
                    PrimaryButton(onPressed: () => oc.moveToNextPage(), text: 'Далее'),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

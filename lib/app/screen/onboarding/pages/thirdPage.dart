import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../app.dart';

class ThirdPage extends StatelessWidget {
  const ThirdPage({super.key});

  @override
  Widget build(BuildContext context) {
    var heightBothWidgets = SizedBox(height: 16.h);
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(image: AssetImage(MyAssetsImages.thirdOnBoardingImg), fit: BoxFit.fill),
      ),
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Stack(
            children: [
              Positioned(
                bottom: MediaQuery.of(context).viewInsets.bottom + 30,
                left: 24,
                right: 24,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Программа мероприятий',
                      style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontFamily: Constants.fontName,
                          fontSize: 24.sp,
                          color: ThemeColors.uiWhiteColor),
                      textAlign: TextAlign.center,
                    ),
                    heightBothWidgets,
                    Text(
                      'Всегда под рукой актуальная информация о программе мероприятия',
                      style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontFamily: Constants.fontName,
                          fontSize: 16.sp,
                          color: ThemeColors.uiWhiteColor),
                      textAlign: TextAlign.center,
                    ),
                    heightBothWidgets,
                    PrimaryButton(
                        onPressed: () => Get.to(
                              () => const DashboardScreen(),
                              transition: Transition.upToDown,
                              duration: const Duration(milliseconds: 1000),
                            ),
                        text: 'Начать работу'),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

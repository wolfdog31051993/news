import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_app/app/app.dart';

class ProfileCard extends StatelessWidget {
  const ProfileCard({super.key});
// 'https://i.pinimg.com/474x/08/f9/1e/08f91e7dca7010f6486d14dca1db4686.jpg'
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(12.r)),
        color: ThemeColors.bgGray4Color,
      ),
      child: Row(
        children: [
          Container(
            width: 100.w,
            height: 100.h,
            padding: const EdgeInsets.all(18),
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              color: ThemeColors.bgGray2Color,
              image: DecorationImage(
                image: NetworkImage('https://i.pinimg.com/474x/08/f9/1e/08f91e7dca7010f6486d14dca1db4686.jpg'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(width: 15.w),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Bayram Komekow',
                style: TextStyle(
                  color: ThemeColors.uiBlackColor,
                  fontWeight: FontWeight.w700,
                  fontFamily: Constants.fontName,
                  fontSize: 18.sp,
                ),
              ),
              SizedBox(height: 15.h),
              Container(
                padding: const EdgeInsets.all(15.0),
                decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.all(Radius.circular(18.r)),
                    color: ThemeColors.uiBlackColor),
                child: Text(
                  'IDL 7564',
                  style: TextStyle(
                    color: ThemeColors.uiWhiteColor,
                    fontWeight: FontWeight.w700,
                    fontFamily: Constants.fontName,
                    fontSize: 15.sp,
                  ),
                ),
              ),
            ],
          ),
          const Spacer(),
          IconButton(
            onPressed: () {},
            icon: const Icon(
              Icons.navigate_next,
              color: ThemeColors.bgGray1Color,
              size: 50,
            ),
          )
        ],
      ),
    );
  }
}

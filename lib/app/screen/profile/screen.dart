import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_app/app/core/core.dart';
import 'package:test_app/app/screen/profile/profile.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    var height = SizedBox(height: 15.h);
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'мой профиль',
                  style: TextStyle(
                    fontFamily: Constants.fontName,
                    fontWeight: FontWeight.w700,
                    fontSize: 32.sp,
                  ),
                ),
                height,
                const ProfileCard(),
                height,
                Text(
                  'Основное',
                  style: TextStyle(
                    fontSize: 18.sp,
                    fontWeight: FontWeight.w400,
                    fontFamily: Constants.fontName,
                    color: ThemeColors.bgGray1Color,
                  ),
                ),
                cardsContainer(Icons.notifications_none_outlined, 'Центр уведомлений'),
                cardsContainer(Icons.calendar_month, 'Мои мероприятия'),
                cardsContainer(Icons.work, 'Сервисы'),
                cardsContainer(Icons.verified_outlined, 'Статус бейдж и ТС'),
                cardsContainer(Icons.settings, 'Настройки аккаунта'),
                height,
                cardsContainer(null, 'Выйти из аккаунта'),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Container cardsContainer(IconData? icon, String tittle) {
    return Container(
      margin: EdgeInsets.only(top: 15.h),
      padding: const EdgeInsets.all(12.0),
      decoration: BoxDecoration(color: ThemeColors.bgGray4Color, borderRadius: BorderRadius.all(Radius.circular(12.r))),
      child: Row(
        children: [
          icon != null ? Icon(icon, color: ThemeColors.primaryColor) : const SizedBox(),
          icon != null ? SizedBox(width: 10.h) : const SizedBox(),
          Text(
            tittle,
            style: TextStyle(
                fontSize: 18.sp,
                fontFamily: Constants.fontName,
                fontWeight: FontWeight.w700,
                color: icon != null ? ThemeColors.uiBlackColor : ThemeColors.primaryColor),
          ),
          const Spacer(),
          IconButton(
            onPressed: () {},
            icon: Icon(
              icon != null ? Icons.navigate_next : Icons.logout,
              color: ThemeColors.bgGray1Color,
            ),
          )
        ],
      ),
    );
  }
}

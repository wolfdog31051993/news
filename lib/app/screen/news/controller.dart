import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_app/app/screen/news/detail/screen.dart';
import 'package:test_app/app/screen/screen.dart';

class NewsController extends GetxController {
  final state = NewsState();

  void onClickCategories(value) {
    state.isClicked.value = value;
  }

  void onClickNews(context, newsModel) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (context) {
          return FractionallySizedBox(
            heightFactor: 1,
            child: NewsDetail(newsModel: newsModel),
          );
        });
    // Navigator.of(context).push(MaterialPageRoute(
    //     builder: (BuildContext context) {
    //       return NewsDetail(newsModel: newsModel);
    //     },
    //     fullscreenDialog: true));
  }
}

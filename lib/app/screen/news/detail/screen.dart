import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:test_app/app/app.dart';

class NewsDetail extends StatelessWidget {
  final NewsModel newsModel;
  const NewsDetail({super.key, required this.newsModel});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<NewsController>(
      init: NewsController(),
      builder: (nc) {
        return Scaffold(
          body: SafeArea(
            child: Container(
              height: 0.9.sh,
              margin: EdgeInsets.only(top: 25.h),
              child: Column(
                children: [
                  Container(
                    width: 1.sw * 0.3,
                    height: 5.h,
                    color: ThemeColors.uiWhiteColor,
                  ),
                  SizedBox(height: 10.h),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 24.0),
                    decoration: BoxDecoration(
                      color: ThemeColors.uiWhiteColor,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(12.r),
                        topLeft: Radius.circular(12.r),
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        topHeader(context),
                        Align(alignment: Alignment.center, child: imageBox()),
                        SizedBox(height: 15.h),
                        Text(
                          newsModel.title!,
                          style: TextStyle(
                            fontSize: 21.sp,
                            fontFamily: Constants.fontName,
                            fontWeight: FontWeight.w700,
                            color: ThemeColors.uiBlackColor,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: 15.h),
                        Text(
                          newsModel.date!,
                          style: TextStyle(
                            fontSize: 12.sp,
                            fontFamily: Constants.fontName,
                            fontWeight: FontWeight.w500,
                            color: ThemeColors.uiBlackColor,
                          ),
                          textAlign: TextAlign.left,
                        ),
                        SizedBox(height: 15.h),
                        Text(
                          newsModel.title!,
                          style: TextStyle(
                            fontSize: 19.sp,
                            fontFamily: Constants.fontName,
                            fontWeight: FontWeight.w700,
                            color: ThemeColors.bgGray1Color,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Container imageBox() {
    return Container(
      height: 168.h,
      margin: EdgeInsets.only(top: 15.h),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(16.r),
        ),
        image: DecorationImage(
          image: NetworkImage(newsModel.img!),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Row topHeader(context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        const SizedBox(),
        Text(
          'Новость',
          style: TextStyle(
            fontSize: 18.sp,
            fontFamily: Constants.fontName,
            fontWeight: FontWeight.w700,
          ),
        ),
        GestureDetector(
          onTap: () => Navigator.of(context).pop(),
          child: const SizedBox(
            child: CircleAvatar(
              backgroundColor: ThemeColors.infButtonsColor,
              child: Icon(
                Icons.close,
                color: ThemeColors.uiBlackColor,
              ),
            ),
          ),
        )
      ],
    );
  }
}

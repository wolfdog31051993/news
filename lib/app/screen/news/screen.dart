import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:test_app/app/screen/screen.dart';

class NewsScreen extends StatelessWidget {
  const NewsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<NewsController>(
      init: NewsController(),
      builder: (nc) {
        return SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 24.0),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      SizedBox(
                        height: 40.h,
                        child: ListView.builder(
                          padding: EdgeInsets.zero,
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          physics: const BouncingScrollPhysics(),
                          itemCount: nc.state.categoriesList.length,
                          itemBuilder: (context, index) {
                            nc.state.isClicked.value = nc.state.categoriesList.first;
                            return NewsCategoriesItem(
                              title: nc.state.categoriesList[index],
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 15.h),
                child: ListView.builder(
                  padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 10),
                  shrinkWrap: true,
                  physics: const BouncingScrollPhysics(),
                  itemCount: nc.state.newsList.length,
                  itemBuilder: (context, index) {
                    return NewsItem(newsModel: nc.state.newsList[index]);
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}

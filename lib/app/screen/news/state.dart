import 'package:get/get.dart';
import 'package:test_app/app/app.dart';

class NewsState {
  RxBool isLoading = false.obs;
  RxString isClicked = ''.obs;

  List<String> categoriesList = [
    'Все',
    'Глобальные тренды',
    'Отрасли чего - то',
    'и так далее',
  ];

  RxList<NewsModel> newsList = <NewsModel>[
    NewsModel(
      title:
          'Последние разработки в области энергоэффективности, инженерных систем, архитектуры и дизайна: что покажет Россия на выставке UzBuild',
      desc:
          '28 февраля в Ташкенте Российский экспортный центр (входит в ВЭБ.РФ) впервые открыл российский павильон на крупнейшей строительной выставке в Узбекистане.',
      img:
          'https://roscongress.org/upload/resize_cache/iblock/8ba/9irx68lp7sfovepgn0o11xqd71q6whtl/1440_956_1/1b31b80f1cbdfb90e6834e054345ed91.jpg',
      date: '27 фев 2023 5 min',
    ),
    NewsModel(
      title: 'Стратегии поддержки и новые горизонты: в преддверии Российского форума дизайна и моды',
      desc:
          '28 февраля в Ташкенте Российский экспортный центр (входит в ВЭБ.РФ) впервые открыл российский павильон на крупнейшей строительной выставке в Узбекистане.',
      img: 'https://master-forum.ru/wp-content/uploads/2023/05/BuildExpo-Uzbekistan-2023-01.jpg',
      date: '27 фев 2023 5 min',
    ),
    NewsModel(
      title: 'Почему нет санкций на производство титана? 27 фев 2023 5 минут',
      desc:
          '28 февраля в Ташкенте Российский экспортный центр (входит в ВЭБ.РФ) впервые открыл российский павильон на крупнейшей строительной выставке в Узбекистане.',
      img: 'https://master-forum.ru/wp-content/uploads/2023/05/BuildExpo-Uzbekistan-2023-03.jpg',
      date: '27 фев 2023 5 min',
    ),
    NewsModel(
      title: 'Почему нет санкций на производство титана? 27 фев 2023 5 минут',
      desc:
          '28 февраля в Ташкенте Российский экспортный центр (входит в ВЭБ.РФ) впервые открыл российский павильон на крупнейшей строительной выставке в Узбекистане.',
      img:
          'https://roscongress.org/upload/resize_cache/iblock/8ba/9irx68lp7sfovepgn0o11xqd71q6whtl/1440_956_1/1b31b80f1cbdfb90e6834e054345ed91.jpg',
      date: '27 фев 2023 5 min',
    ),
  ].obs;
}

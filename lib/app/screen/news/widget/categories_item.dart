import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:test_app/app/core/theme/theme.dart';
import 'package:test_app/app/screen/screen.dart';

class NewsCategoriesItem extends StatelessWidget {
  final String title;
  const NewsCategoriesItem({super.key, required this.title});

  @override
  Widget build(BuildContext context) {
    return GetX<NewsController>(
      init: NewsController(),
      builder: (nc) {
        return GestureDetector(
          onTap: () => nc.onClickCategories(title),
          child: Container(
            padding: const EdgeInsets.all(10.0),
            margin: EdgeInsets.only(right: 10.w),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(12.r)),
              color: nc.state.isClicked.value == title ? ThemeColors.primaryColor : ThemeColors.bgGray4Color,
            ),
            child: Text(
              title,
              style: TextStyle(
                color: nc.state.isClicked.value == title ? ThemeColors.uiWhiteColor : ThemeColors.uiBlackColor,
                fontSize: 14.sp,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        );
      },
    );
  }
}

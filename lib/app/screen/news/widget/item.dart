import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../app.dart';

class NewsItem extends StatelessWidget {
  final NewsModel newsModel;
  const NewsItem({super.key, required this.newsModel});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<NewsController>(
      init: NewsController(),
      builder: (nc) {
        return GestureDetector(
          onTap: () => nc.onClickNews(context, newsModel),
          child: Container(
            height: 129.h,
            padding: const EdgeInsets.all(12.0),
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(12.0),
                ),
                color: ThemeColors.bagRoundColor),
            margin: EdgeInsets.only(bottom: 10.h),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: 1.sw * 0.4,
                      child: Text(
                        newsModel.title!,
                        style: TextStyle(
                            color: ThemeColors.uiBlackColor,
                            fontFamily: Constants.fontName,
                            fontSize: 15.sp,
                            overflow: TextOverflow.ellipsis,
                            fontWeight: FontWeight.w700),
                        maxLines: 3,
                      ),
                    ),
                    const Spacer(),
                    Text(
                      newsModel.date!,
                      style: TextStyle(
                          color: ThemeColors.uiBlackColor,
                          fontFamily: Constants.fontName,
                          fontSize: 12.sp,
                          overflow: TextOverflow.ellipsis,
                          fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
                Container(
                  width: 96.w,
                  height: 97.h,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(16.r)),
                    image: DecorationImage(
                      image: NetworkImage(newsModel.img!),
                      fit: BoxFit.cover,
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}

import 'package:badges/badges.dart';
import 'package:badges/badges.dart' as badges;
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../app.dart';

class NotificationBtn {
  static notificationBtn() {
    return badges.Badge(
      showBadge: true,
      badgeColor: ThemeColors.primaryColor,
      animationType: BadgeAnimationType.scale,
      shape: BadgeShape.circle,
      alignment: Alignment.center,
      position: BadgePosition.topEnd(end: -5, top: -5),
      badgeContent: ConstrainedBox(
        constraints: BoxConstraints(minWidth: 15.w, minHeight: 15.h),
        child: Text(
          '1',
          style: TextStyle(
            fontSize: 12.sp,
            fontWeight: FontWeight.w700,
            fontFamily: Constants.fontName,
            color: ThemeColors.uiWhiteColor,
          ),
          textAlign: TextAlign.center,
        ),
      ),
      child: Container(
        width: 40.w,
        height: 40.h,
        decoration: BoxDecoration(
          color: ThemeColors.infButtonsColor,
          borderRadius: BorderRadius.all(
            Radius.circular(
              12.sp,
            ),
          ),
        ),
        child: const Icon(
          Icons.notifications_none_outlined,
          size: 35,
          color: ThemeColors.uiBlackColor,
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../app.dart';

class AboutBtn {
  static GestureDetector aboutFondButton() {
    return GestureDetector(
      onTap: () {},
      child: Container(
        alignment: Alignment.center,
        width: 109.w,
        height: 40.h,
        decoration: BoxDecoration(
          color: ThemeColors.infButtonsColor,
          borderRadius: BorderRadius.all(
            Radius.circular(12.sp),
          ),
        ),
        child: Text(
          'О фонде',
          style: TextStyle(
              color: ThemeColors.uiBlackColor,
              fontFamily: Constants.fontName,
              fontSize: 16.sp,
              fontWeight: FontWeight.w700),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}

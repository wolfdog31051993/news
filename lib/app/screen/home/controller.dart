import 'package:get/get.dart';
import 'package:test_app/app/app.dart';

class HomeController extends GetxController {
  final state = HomeState();

  void onToggle(index) => state.tabCounter.value = index;
}

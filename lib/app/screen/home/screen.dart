import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:test_app/app/app.dart';
import 'package:toggle_switch/toggle_switch.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return GetX<HomeController>(
      init: HomeController(),
      builder: (hc) {
        return Scaffold(
          appBar: AppBar(
            centerTitle: true,
            backgroundColor: Colors.transparent,
            elevation: 0,
            automaticallyImplyLeading: false,
            title: Row(
              children: [
                SizedBox(
                  width: 42.w,
                  height: 44.h,
                  child: Image.asset(MyAssetsImages.logoIc),
                ),
                const Spacer(),
                AboutBtn.aboutFondButton(),
                SizedBox(width: 10.w),
                NotificationBtn.notificationBtn(),
                SizedBox(height: 20.h),
              ],
            ),
          ),
          body: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(23.0),
                child: Align(
                  alignment: Alignment.center,
                  child: Container(
                    width: 335.w,
                    height: 36.h,
                    padding: const EdgeInsets.all(2),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(12.sp),
                      ),
                      color: ThemeColors.bgGray4Color,
                    ),
                    child: ToggleSwitch(
                      customWidths: const [165.0, 165.0],
                      cornerRadius: 12.0,
                      labels: const ['Мероприятие', 'Новости'],
                      activeFgColor: ThemeColors.uiBlackColor,
                      inactiveFgColor: ThemeColors.uiBlackColor,
                      initialLabelIndex: hc.state.tabCounter.value,
                      customTextStyles: [
                        TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 15.sp,
                          fontFamily: Constants.fontName,
                        ),
                        TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 15.sp,
                          fontFamily: Constants.fontName,
                        )
                      ],
                      activeBgColor: const [
                        ThemeColors.uiWhiteColor,
                        ThemeColors.uiWhiteColor,
                      ],
                      inactiveBgColor: Colors.transparent,
                      onToggle: (index) => hc.onToggle(index),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 10.h),
              if (hc.state.tabCounter.value == 0)
                const Expanded(child: EventsScreen())
              else if (hc.state.tabCounter.value == 1)
                const Expanded(child: NewsScreen())
            ],
          ),
        );
      },
    );
  }
}

library screen;

export 'login/login.dart';
export 'onboarding/onboarding.dart';
export 'dashboard/dashboard.dart';
export 'home/home.dart';
export 'events/events.dart';
export 'news/news.dart';
export 'profile/profile.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_app/app/app.dart';

class DashboardState {
  RxInt selectedIndex = 0.obs;

  List<Widget> tabItems = [
    const HomeScreen(),
    const Center(child: Text("1")),
    const Center(child: Text("2")),
    const ProfileScreen()
  ];
}

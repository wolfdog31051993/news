import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_app/app/screen/screen.dart';

class DashboardController extends GetxController {
  final state = DashboardState();

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  void onSelectedIndex(index) {
    state.selectedIndex.value = index;
  }
}

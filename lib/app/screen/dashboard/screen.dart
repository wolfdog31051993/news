import 'package:flashy_tab_bar2/flashy_tab_bar2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:test_app/app/app.dart';
import 'package:test_app/app/screen/screen.dart';

class DashboardScreen extends StatelessWidget {
  const DashboardScreen({super.key});

  @override
  Widget build(BuildContext context) {
    var style = TextStyle(fontFamily: Constants.fontName, fontSize: 12.sp, fontWeight: FontWeight.w700);
    return GetX<DashboardController>(
      init: DashboardController(),
      builder: (dc) {
        return Scaffold(
          body: Center(
            child: dc.state.tabItems[dc.state.selectedIndex.value],
          ),
          bottomNavigationBar: FlashyTabBar(
            animationCurve: Curves.linear,
            selectedIndex: dc.state.selectedIndex.value,
            iconSize: 30,
            showElevation: false, // use this to remove appBar's elevation
            onItemSelected: (index) => dc.onSelectedIndex(index),
            items: [
              FlashyTabBarItem(
                icon: const Icon(Icons.event),
                title: Text('главная', style: style),
              ),
              FlashyTabBarItem(
                icon: const Icon(Icons.calendar_month),
                title: Text('мероприятия', style: style),
              ),
              FlashyTabBarItem(
                icon: const Icon(Icons.chat),
                title: Text('чат', style: style),
              ),
              FlashyTabBarItem(
                icon: const Icon(Icons.person),
                title: Text('мой профиль', style: style),
              ),
            ],
          ),
        );
      },
    );
  }
}

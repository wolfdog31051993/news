import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:test_app/app/app.dart';

class EventsScreen extends StatelessWidget {
  const EventsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return GetX<EventsController>(
        init: EventsController(),
        builder: (ec) {
          return ec.state.isLoading.value
              ? const CustomLoader()
              : ec.state.eventsList.isNotEmpty
                  ? Column(
                      children: [
                        SizedBox(
                          height: 1.sh * 0.6,
                          child: CarouselSlider.builder(
                            options: CarouselOptions(
                                height: 1.sh * 0.6,
                                initialPage: 0,
                                aspectRatio: 16 / 9,
                                viewportFraction: 0.87,
                                autoPlay: true,
                                reverse: false,
                                enableInfiniteScroll: true,
                                autoPlayInterval: const Duration(seconds: 5),
                                autoPlayAnimationDuration: const Duration(milliseconds: 2000),
                                pauseAutoPlayOnTouch: true,
                                scrollDirection: Axis.horizontal,
                                onPageChanged: (value, _) {
                                  ec.onChangePage(value);
                                }),
                            itemCount: ec.state.eventsList.length,
                            itemBuilder: (_, index, pageViewIndex) {
                              return Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(24.r),
                                    ),
                                  ),
                                  child: EventsItem(eventsModel: ec.state.eventsList[index]));
                            },
                          ),
                        ),
                        SizedBox(height: 20.h),
                        AnimatedSmoothIndicator(
                          activeIndex: ec.state.pageIndicator.value,
                          count: ec.state.eventsList.length,
                          effect: const ExpandingDotsEffect(
                            spacing: 10,
                            radius: 50.0,
                            dotWidth: 10.0,
                            dotHeight: 10.0,
                            paintStyle: PaintingStyle.fill,
                            dotColor: ThemeColors.secondaryColor,
                            activeDotColor: ThemeColors.uiBlackColor,
                          ),
                        )
                      ],
                    )
                  : const Center(
                      child: Text('На данный момент нет ни какоxй мероприятий'),
                    );
        });
  }
}

import 'package:get/get.dart';
import 'package:test_app/app/app.dart';

class EventsController extends GetxController {
  final state = EventsState();

  void onChangePage(value) => state.pageIndicator.value = value;
}

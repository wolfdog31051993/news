import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../app.dart';

class EventsItem extends StatelessWidget {
  final EventsModel eventsModel;
  const EventsItem({super.key, required this.eventsModel});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 1.sh * 0.9,
      margin: EdgeInsets.symmetric(horizontal: 15.w),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(24.r),
        ),
        color: Colors.black,
      ),
      child: Stack(
        children: [
          Container(
            height: 1.sh * 0.9,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(24.r),
              ),
              image: DecorationImage(image: NetworkImage(eventsModel.img!), fit: BoxFit.cover),
            ),
          ),
          Container(
            height: 1.sh * 0.9,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(24.r),
              ),
              color: ThemeColors.uiBlackColor.withOpacity(0.6),
            ),
          ),
          shareBtn(),
          infoContainer(context)
        ],
      ),
    );
  }

  Align shareBtn() => Align(
        alignment: Alignment.topRight,
        child: GestureDetector(
          onTap: () {},
          child: Container(
              height: 48.h,
              width: 48.w,
              alignment: Alignment.center,
              margin: EdgeInsets.only(right: 24.w, top: 24.h),
              decoration: BoxDecoration(
                color: const Color(0xFFF2F3F5).withOpacity(0.6),
                borderRadius: BorderRadius.all(
                  Radius.circular(12.r),
                ),
              ),
              child: const Icon(
                Icons.redo,
                color: ThemeColors.uiWhiteColor,
              )),
        ),
      );

  Positioned infoContainer(context) {
    var height = SizedBox(height: 15.h);
    return Positioned(
      bottom: MediaQuery.of(context).viewInsets.bottom + 30,
      left: 24,
      right: 24,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            eventsModel.date!,
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 14.sp,
              fontFamily: Constants.fontName,
              color: ThemeColors.uiWhiteColor,
            ),
            textAlign: TextAlign.left,
          ),
          height,
          Text(
            eventsModel.desc!,
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 20.sp,
              fontFamily: Constants.fontName,
              color: ThemeColors.uiWhiteColor,
            ),
            textAlign: TextAlign.left,
          ),
          height,
          Text(
            eventsModel.city!,
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 12.sp,
              fontFamily: Constants.fontName,
              color: ThemeColors.uiWhiteColor,
            ),
            textAlign: TextAlign.left,
          ),
        ],
      ),
    );
  }
}

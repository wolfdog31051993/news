import 'package:get/get.dart';
import 'package:test_app/app/data/data.dart';

class EventsState {
  RxBool isLoading = false.obs;

  RxInt pageIndicator = 0.obs;

  List<EventsModel> eventsList = [
    EventsModel(
      date: '11–13 мая | Экспофорум',
      desc: 'Петербургский международный юридический форум 2023',
      city: 'Санкт-Петербург, Россия',
      img:
          'https://api.rbsmi.ru/attachments/bc008a0a0dfdb681297ff14856df01a1b9a3669a/store/crop/0/0/1280/853/1600/0/0/b796e36541c1109e4179ef6133a00a4925c0f153a29234dd52e8c04ac206/88888888888888888888888.jpg',
    ),
    EventsModel(
        date: '14–17 мая | Экспофорум',
        desc: 'Петербургский международный юридический форум 2023',
        city: 'Санкт-Петербург, Россия',
        img: 'https://cdn.tvspb.ru/storage/wp-content/uploads/2023/05/forum_10_1.jpg__0_0x0.jpg'),
    EventsModel(
        date: '18–21 мая | Экспофорум',
        desc: 'Петербургский международный юридический форум 2023',
        city: 'Санкт-Петербург, Россия',
        img: 'https://i.imgur.com/Dzb42se.jpg'),
  ];
}

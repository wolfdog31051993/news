class NewsModel {
  String? title;
  String? desc;
  String? img;
  String? date;

  NewsModel({this.title, this.desc, this.img, this.date});
}

class EventsModel {
  String? date;
  String? desc;
  String? city;
  String? img;

  EventsModel({this.date, this.desc, this.city, this.img});
}

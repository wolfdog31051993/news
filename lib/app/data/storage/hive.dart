import 'package:hive_flutter/hive_flutter.dart';

class Boxes {
  static const String finishOnBoarding = 'finishOnBoarding';

  static Future<void> initBoxes() async {
    await Hive.initFlutter();

    await Hive.openBox(finishOnBoarding);
  }

  static void close() => Hive.close();
}

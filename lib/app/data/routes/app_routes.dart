class AppRoutes {
  static const String loginScreen = '/loginScreen';
  static const String onBoardingScreen = '/onBoardingScreen';
  static const String dashboardScreen = '/dashboardScreen';
}

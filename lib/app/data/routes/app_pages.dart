import 'package:get/get.dart';

import '../../app.dart';

class AppPages {
  static var pageList = [
    GetPage(name: AppRoutes.loginScreen, page: () => const LoginScreen()),
    GetPage(name: AppRoutes.onBoardingScreen, page: () => const OnBoardingScreen()),
    GetPage(name: AppRoutes.dashboardScreen, page: () => const DashboardScreen()),
  ];
}

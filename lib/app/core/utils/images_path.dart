class MyAssetsImages {
  static const basicFolder = 'assets/images/';
  static const logoIc = '${basicFolder}logo.png';

  //OnBoarding
  static const onBoardingBasicFolder = '${basicFolder}onboarding/';
  static const firstOnBoardingImg = '${basicFolder}onboarding/first_img.jpeg';
  static const secondOnBoardingImg = '${basicFolder}onboarding/second_img.jpeg';
  static const thirdOnBoardingImg = '${basicFolder}onboarding/third_img.jpeg';
}

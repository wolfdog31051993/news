import 'package:flutter/material.dart';

class ThemeColors {
  static const primaryColor = Color(0xFFC60033);
  static const secondaryColor = Color(0xFFFDF0F4);
  static const bgGray1Color = Color(0xFF778A9B);
  static const bgGray2Color = Color(0xFFEBEDF0);
  static const bgGray4Color = Color(0xFFF5F5F5);
  static const uiBlackColor = Color(0xFF212122);
  static const uiWhiteColor = Color(0XFFFFFFFF);
  static const infButtonsColor = Color(0xFFF5F5F5);
  static const bagRoundColor = Color(0xFFE4E5E8);
}
